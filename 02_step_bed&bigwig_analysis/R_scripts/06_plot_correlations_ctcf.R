# Creation date: 2023/04/14
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: For histones marks. From table of correlation (file), plot means correlation and determination coefficients
# and plot each observed/predicted data correlation coefficients per condition.
# Create png file for plots to show.
# Modules versions: R version 4.2.1, ggh4x 0.2.4, ggplot2 3.4.1, ggpubr 0.6.0

library(ggh4x)
library(ggplot2)
library(ggpubr)


theme_update(plot.title = element_text(hjust = 0.5))  # to set centered title for all plots
theme_update(panel.background=element_rect(fill="white", color='black'))
theme_update(panel.grid=element_blank())

### output table analysis, plot correlations file
# set path to save plots
save_path <- "C:/Users/nomaillard/Documents/predictions_from_Sscrofa_refseq/CTCF_binding/"
# set and read correlations file
corr_file <- "C:/Users/nomaillard/Documents/CTCF_binding/correlations_enformer.txt"
correlations <- read.csv(corr_file, sep='\t', header=FALSE, col.names=c('condition', 'observed', 'predicted', 'correlation', 'mark', 'tissue'))
# calculate coefficient of determination
correlations$determination <- correlations$correlation^2

# create column to store shortened prediction file names
# this step is usefull to shorten plots legends and labels (and make it more readable)
# each mark_tissue_number fits with prediction filename
correlations$pred_short <- NA
cnt <- 0
for (pred_name in unique(correlations$predicted)){
  cnt <- cnt+1
  # add mark_tissuecnt in column
  for (i in 1:(nrow(correlations[which(correlations$predicted==pred_name),]))){
    correlations$pred_short[which(correlations$predicted==pred_name)][i] <- paste0(correlations$mark_tissue[which(correlations$predicted==pred_name)][1], cnt)
  }
}

# plot data as raw, (no condition, no grouping...)
ggplot(correlations, aes(x=condition, y=correlation)) +
  geom_point() +
  ylim(0, 1)

# plot by mark and color by tissue
png(filename=paste0(save_path, "correlations_enformer_ctcf.png"), width=700, height=500)
ggplot(correlations, aes(x=mark, y=correlation)) +
  geom_point(position=position_dodge(width=0.3), aes(color=tissue, group=tissue)) +
  ylim(0, 1) +
  labs(title="Correlations distribution by mark for each tissue")
dev.off()

# plot by tissue and mark
# extract colors except greys (to optimize contrast)
color = grDevices::colors()[grep('gr(a|e)y', grDevices::colors(), invert = T)]
# changed [start:stop] interval to have more contrasted colors.
# The most important is the number of colors which must fit to input data number.
palettes <- color[19:(19+(length(unique(correlations$pred_short))))]
### plot each correlation. 1 point = 1 correlation for a mark/tissue/observed/predicted combinaison
## x=observed, color=predicted
# with legend
png(filename=paste0(save_path, "correlations_enformer_detailed_by_biosampl_legend.png"), width=700, height=500)
ggplot(correlations, aes(x=observed, y=correlation, color=pred_short)) +
  geom_point() +
  scale_color_manual(values=palettes) +
  ylim(0,1) +
  facet_grid(mark~tissue, scales='free') +
  theme(panel.grid.major=element_blank(), panel.background=element_rect(fill="#696969"), strip.background = element_rect(fill = '#cfcfcf'), axis.text.x = element_text(angle = 90, vjust=0.5)) +
  labs(title="Correlations for each tissue", x="observed data samples names")
dev.off()
# without legend
png(filename=paste0(save_path, "correlations_enformer_detailed_by_biosampl_nolegend.png"), width=700, height=500)
ggplot(correlations, aes(x=observed, y=correlation, color=pred_short)) +
  geom_point() +
  scale_color_manual(values=palettes) +
  ylim(0,1) +
  facet_grid(mark~tissue, scales='free') +
  theme(legend.position='none', panel.grid.major=element_blank(), panel.background=element_rect(fill="#696969"), strip.background = element_rect(fill = '#cfcfcf'), axis.text.x = element_text(angle = 90, vjust=0.5)) +
  labs(title="Correlations for each tissue", x="observed data samples names")
dev.off()

## x=pred, color=observed
# with legend
png(filename=paste0(save_path, "correlations_enformer_detailed_by_predsampl_legend.png"), width=700, height=500)
ggplot(correlations, aes(x=pred_short, y=correlation, color=observed)) +
  geom_point() +
  scale_color_manual(values=palettes) +
  ylim(0,1) +
  facet_grid(mark~tissue, scales='free') +
  theme(panel.grid.major=element_blank(), panel.background=element_rect(fill="#696969"), strip.background = element_rect(fill = '#cfcfcf'), axis.text.x = element_text(angle = 90, vjust=0.5)) +
  labs(title="Correlations for each tissue", x="observed data samples names")
dev.off()
# without legend
png(filename=paste0(save_path, "correlations_enformer_detailed_by_predsampl_nolegend.png"), width=700, height=500)
ggplot(correlations, aes(x=pred_short, y=correlation, color=observed)) +
  geom_point() +
  scale_color_manual(values=palettes) +
  ylim(0,1) +
  facet_grid(mark~tissue, scales='free') +
  theme(legend.position='none', panel.grid.major=element_blank(), panel.background=element_rect(fill="#696969"), strip.background = element_rect(fill = '#cfcfcf'), axis.text.x = element_text(angle = 90, vjust=0.5)) +
  labs(title="Correlations for each tissue", x="observed data samples names")
dev.off()

# plot correlations according to mark_tissue combinaisons
ggplot(correlations, aes(x=tissue, y=correlation)) +
  geom_point() +
  ylim(0, 1) +
  theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1)) +
  labs(title="Correlations for each mark/tissue combinaison")

# calculate then plot mean correlations for each mark/tissue combinaison (1st plot: without std)
corr_mean <- data.frame(unique(correlations$tissue))
colnames(corr_mean) <- c('tissue')

for (tissue in unique(correlations$tissue)){
  corr_mean$mean_correlation[which(corr_mean$tissue==tissue)] <- mean(correlations$correlation[which(correlations$tissue==tissue)])
}
# this plot is only to check that means are same than next plot results
ggplot(corr_mean, aes(x=tissue, y=mean_correlation)) +
  geom_point() +
  ylim(0, 1) +
  theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1)) +
  labs(title="Mean correlations for each tissue", y="mean correlation")

png(filename=paste0(save_path, "correlations_enformer_by_tissue.png"), width=700, height=500)
# plot mean coefficients of correlation with standard errors
ggplot(correlations, aes(x=tissue, y=correlation, width=0.5)) +
  stat_summary(func='mean', geom='point') +
  stat_summary(func='sd', geom='errorbar') +
  ylim(0, 1) +
  theme(axis.text.x=element_text(angle = 90, vjust=0.5, hjust=1), axis.text.y=element_text(size=15), axis.ticks=element_blank()) +
  force_panelsizes(rows=unit(3, "cm"), cols=unit(3, "cm")) +
  labs(title="Mean correlations for each tissue", y="mean correlation")
dev.off()

# plot mean coefficients of determination with standard errors
png(filename=paste0(save_path, "determinations_enformer_by_tissue.png"), width=700, height=500)
ggplot(correlations, aes(x=tissue, y=determination)) +
  stat_summary(func='mean', geom='point') +
  stat_summary(func='sd', geom='errorbar') +
  ylim(0, 1) +
  theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1)) +
  labs(title="Mean determination coefficient for each tissue", y="mean determination coefficient")
dev.off()

