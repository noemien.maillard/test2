# Creation date: 2023/03/09
# Last review: 2023/07/04
# Note on last review: the script runs correctly except it stops for a file
# 'isSingleString(path)' error, probably due to windows and rtracklayer conflict...
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Generate bw files from enformer prediction data
# Modules versions: R version 4.2.1, GenomicRanges 1.50.2, rtracklayer 1.58.0, stringr 1.5.0

library(GenomicRanges)
library(rtracklayer)
library(stringr)

# set input directory for bins and enformer predictions files
setwd(dir = "C:/Users/nomaillard/Documents/predictions_from_Sscrofa_refseq/enformer")
enformer <- read.csv('20220722_Analyse_listQTL1_2429_SNP_v11.enformer_windows_393216b.csv', header=T, sep=',')
bins <- read.csv('20220722_Analyse_listQTL1_2429_SNP_v11.enformer_bins_128b.bed', header=F, sep='\t')

# store chr and positions commun for all GRanges objects created in loop
grnames <- Rle(bins$V1)
grpos <- IRanges(start=bins$V2, end=bins$V3)

# set path where you want to save your data and init your loop (can be same or different)
save_path <- "C:/Users/nomaillard/Documents/predictions_from_Sscrofa_refseq/enformer/bigwig/"

# for each column, generate and save bigwig file
for (i in 1:length(enformer)){
  # store scores and init GRanges object
  scores <- enformer[, i]
  gr <- GRanges(
    seqnames=grnames,
    ranges=grpos,
    score=scores
  )
  # mandatory line for exporting
  seqlengths(gr) <- max(split(end(gr), seqnames(gr)))
  # get file name and add bw extention
  exp_cond <- names(enformer[i])
  file_name <- paste0(save_path, exp_cond, ".bw")
  # export to bigwig format
  export.bw(object=gr, con=file_name, format="bw")
}
