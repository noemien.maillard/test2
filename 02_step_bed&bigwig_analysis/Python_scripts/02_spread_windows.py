#!/bin/python
# Creation date: 2023/03/29
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Transform input tsv with cols="start", "end", "score" into output tsv with cols="position", "score".
# each start-stop pair is spread and blanks are filled with score=0

import argparse
import pandas as pd


def get_options():
    """Set up and parse arguments.

    Returns:
        Namespace: Parser with arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        help='Input file bed format. sep="\t"'
    )
    parser.add_argument(
        '--output',
        help='Output file, bed format. sep="\t".'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Table separator, default to "\t".'
    )
    return parser.parse_args()


def main(input: str, output: str, sep: str):
    """Write a pandas dataframe with spread ranges and fill blanks with 0 then write tsv file.

    Args:
        input (str): Input tsv file name.
        output (str): Output tsv file name.
    """
    # store input data into pandas df
    table = pd.read_csv(input, sep=sep)
    # init output pandas df and fill it with positions (from start to end in input file) and scores to 0.
    df = pd.DataFrame(columns=['position', 'score'])
    df['position'] = range(int(table['start'].iloc[0]), int(table['end'].iloc[-1]))
    df['score'] = 0
    for _, line in table.iterrows():
        # for each line of input file, positions are associated with score between start and end.
        start_pos = line['start']
        end_pos = line['end']
        # copy score from start to end
        df.loc[df['position'].between(start_pos, end_pos), 'score'] = line['score']
    df.to_csv(output, index=False, sep=sep)


if __name__ == "__main__":
    args = get_options()
    main(input=args.input, output=args.output, sep=args.sep)
            