#!/bin/python
# Creation date: 2023/03/08
# Last review: 2023/07/04
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Cut region of interest (300kb for the QTL1_porc project) from bed file and past firsts 3 columns.


import argparse
import pandas as pd


def get_options() -> argparse.Namespace:
    """Set up and parse arguments.

    Returns:
        Namespace: Parser with arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--input',
        help="Input bed file. Bed file is considered with no header and no index."
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Output file to save in bed format (3 firsts columns corresponding to chr, min and max).'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Input and output separator. Default to "\t"'
    )
    parser.add_argument(
        '--chr',
        help='Chromosome to cut. chrX or X (with X as the number of the chromosome) can be used.'
    )
    parser.add_argument(
        '--min',
        type=int,
        help='Minimum limit to cut included.'
    )
    parser.add_argument(
        '--max',
        type=int,
        help="Maximum limit to cut included."
    )
    return parser.parse_args()


def filter_on_chr(input_bed: pd.DataFrame, chr: str) -> pd.DataFrame:
    """Filter dataframe on chromosome.

    Args:
        input_bed (pd.DataFrame): dataframe to filter on chromosome column (index=0).
        chr (str): chromosome to keep.

    Returns:
        pd.DataFrame: Filtered dataframe.
    """
    # if chr column in df is 'chrX' format and chr is 'X' format, look for 'chrX' format
    if input_bed.iloc[0,0].startswith('chr') and not chr.startswith('chr'):
            filtered_bed = input_bed.loc[input_bed.iloc[:,0] == f'chr{chr}']
    # if chr column in df is 'X' format and chr is 'chrX' format, look for 'X' format
    elif not input_bed.iloc[0,0].startswith('chr') and chr.startswith('chr'):
            filtered_bed = input_bed.loc[input_bed.iloc[:,0] == chr[3:]]
    # else, chr column in df and chr are in same format so look for chr in df
    else:
            filtered_bed = input_bed.loc[input_bed.iloc[:,0] == chr]

    return filtered_bed


def filter_on_positions(input_bed: pd.DataFrame, min: int, max: int) -> pd.DataFrame:
    """Filter dataframe on min and max positions.

    Args:
        input_bed (pd.DataFrame): dataframe to filter on positions columns (min_index=1, max_index=2)
        min (int): Minimum position to keep.
        max (int): Maximum position to keep.

    Returns:
        pd.DataFrame: Filtered dataframe.
    """
    # keep dataframe with min column > min-1 and max column < max+1
    return input_bed[(input_bed.iloc[:,1] > min-1) & (input_bed.iloc[:,2] < max+1)]


def main(input: str, output: str, sep: str, chr: str, min: int, max: int):
    """Write bed file from input bed file with chromosome and min/max positions restrictions.
    Keep input file intact.

    Args:
        input (str): Input bed file name.
        output (str): Output bed file name.
        chr (str): Chromosome considering "chrX" format with X the number of the chromosome.
        min (int): The start position to limit the output file.
        max (int): The end position to limit the output file.
    """
    # load input file
    input_bed = pd.read_csv(input, sep=sep, header=None)
    # filter on chromosome
    filtered_bed_on_chr = filter_on_chr(input_bed=input_bed, chr=chr)
    # filter on min and max positions
    filtered_bed_on_pos = filter_on_positions(input_bed=filtered_bed_on_chr, min=min, max=max)
    # save 3 firsts columns
    filtered_bed_on_pos.iloc[:,:3].to_csv(output, sep=sep, index=False, header=False)


if __name__ == '__main__':
    args = get_options()
    main(input=args.input, output=args.output, sep=args.sep, chr=args.chr, min=args.min, max=args.max)