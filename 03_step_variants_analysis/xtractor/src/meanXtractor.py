# Creation date: 2023/05/02
# Last review: 2023/07/05
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Calcul absolute means on all experiment and keep n best absolute means 
# or keep n best absolute mean for each experiment.

from copy import deepcopy
import logging
import pandas as pd


def reorder_cols(df: pd.DataFrame, firsts_cols: list) -> pd.DataFrame:
    """Reorder columns to get info columns at first, then experiment columns.

    Args:
        df (pd.DataFrame): Input dataframe.
        firsts_cols (list): Info columns to keep firsts.

    Returns:
        pd.DataFrame: Reordered dataframe with info columns firsts.
    """
    # store firsts_cols (and their content)
    new_df = df[firsts_cols]
    # drop firsts_cols from original dataframe
    df.drop(columns=firsts_cols, inplace=True)
    # concat info columns with experiments columns and return
    return pd.concat([new_df, df], axis=1)


def calc_snp_means(df: pd.DataFrame) -> pd.DataFrame:
    """Calcul means for all experiments.

    Args:
        df (pd.DataFrame): Input dataframe.

    Returns:
        pd.DataFrame: Dataframe with snp means reorder with info columns first.
    """
    df_means = df.iloc[:,5:]
    df_means['snp_means'] = df_means.mean(axis=1)
    df_means = pd.concat([df.iloc[:,:5], df_means], axis=1)
    firsts_cols = ["CHROM", "POS", "ID", "REF", "ALT", "snp_means"]
    return reorder_cols(df=df_means, firsts_cols=firsts_cols)


def calc_snp_abs_means(df: pd.DataFrame) -> pd.DataFrame:
    """Calcul means on snp absolute scores.

    Args:
        df (pd.DataFrame): Input dataframe.

    Returns:
        pd.DataFrame: Dataframe with means of snp absolute scores.
    """
    # new df to transform scores as absolute scores without modifing original one
    abs_df = df.iloc[:,5:].abs()
    abs_df = pd.concat([df.iloc[:,:5], abs_df], axis=1)
    abs_df['snp_abs_means'] = abs_df.iloc[:,5:].mean(axis=1)
    firsts_cols = ["CHROM", "POS", "ID", "REF", "ALT", "snp_abs_means"]
    return reorder_cols(df=abs_df, firsts_cols=firsts_cols)


def best_col_scores(input: str, output: str, sep: str, n_scores: int):
    """Generate a table with 3 columns (condition, position and score)
    concatenating best absolute scores for each experiment.
    If output file referenced, generate an output file else print in terminal.

    Args:
        input (str): Input file to use as dataframe.
        output (str): Output file name.
        sep (str): Input file separator.
        n_scores (int): Number of (absolute) scores to keep.
    """
    # read input table as dataframe and calculate absolute scores
    df = pd.read_csv(input, sep=sep, engine='python')
    abs_df = df.iloc[:,5:].abs()
    abs_df = pd.concat([df.iloc[:,:5], abs_df], axis=1)
    # output dataframe
    final_df = pd.DataFrame(columns=['condition', 'position', 'score'])
    # for each experiment, take n best absolute scores
    for condition in abs_df.iloc[:,5:].columns:
        largests = abs_df.nlargest(n=n_scores, columns=condition, keep='all').loc[:,['POS', condition]]
        dict_condition = {
            'condition': [],
            'position': [],
            'score': []
        }
        # for each position (with best absolute scores), store condition, position and absolute score in output df
        for position in largests.iterrows():
            dict_condition['condition'].append(condition)
            dict_condition['position'].append(int(position[1][0]))
            dict_condition['score'].append(position[1][1])
        # concat n best absolute scores for the current condition in output df and log it
        final_df = pd.concat([final_df, pd.DataFrame(dict_condition)], ignore_index=True)
        logging.info(f"Condition {condition} stores {len(dict_condition['position'])} positions and associated scores.")
        
    # generate output file
    if output:
        final_df.to_csv(path_or_buf=output, header=True, index=False)
    # print in terminal
    else:
        print(final_df)


def best_means(input: str, output: str, sep: str, n_scores: int):
    """Calculate best absolute means ans best means of absolute scores
    and generate 2 output files (one for each calculation)
    or print in terminal if no output file referenced.

    Args:
        input (str): Input file to use as dataframe.
        output (str): Output file name.
        sep (str): Input file separator.
        n_scores (int): Number of scores to keep.
    """
    # read input table as dataframe
    df = pd.read_csv(input, sep=sep, engine='python')
    # calculate absolute snp means (deepcopy to not modify df)
    df_abs_snp_means = calc_snp_means(df=deepcopy(df))
    df_abs_snp_means['abs_snp_means'] = abs(df_abs_snp_means['snp_means'])
    n_abs_snp_means_df = df_abs_snp_means.nlargest(n=n_scores, columns='abs_snp_means', keep='all')
    # calculate means on absolute snp scores
    df_snp_abs_means = calc_snp_abs_means(df=df)
    n_snp_abs_means_df = df_snp_abs_means.nlargest(n=n_scores, columns='snp_abs_means', keep='all')

    # set output table names
    output_abs_snp_means = output[:-4] + 'BestAbsoluteSnpMeans' + output[-4:]
    output_snp_abs_means = output[:-4] + 'BestSnpAbsoluteMeans' + output[-4:]
    # generate output files
    if output:
        # save table with absolute means calculated on snp scores (positive and negative)
        logging.info(f'{n_abs_snp_means_df.shape[0]} best absolute snp means kept')
        try:
            reorder_cols(df=n_abs_snp_means_df, firsts_cols=["CHROM", "POS", "ID", "REF", "ALT", "snp_means", "abs_snp_means"]).to_csv(path_or_buf=output_abs_snp_means, header=True, index=False)
        except KeyError:
            n_abs_snp_means_df.to_csv(path_or_buf=output_abs_snp_means, header=True, index=False)
        # save table with means calculated on absolute snp scores (positive so)
        logging.info(f'{n_snp_abs_means_df.shape[0]} best snp absolute means kept')
        try:
            reorder_cols(df=n_snp_abs_means_df, firsts_cols=["CHROM", "POS", "ID", "REF", "ALT", "snp_means", "snp_abs_means"]).to_csv(path_or_buf=output_snp_abs_means, header=True, index=False)
        except KeyError:
            n_snp_abs_means_df.to_csv(path_or_buf=output_snp_abs_means, header=True, index=False)
    # print in terminal
    else:
        print(n_abs_snp_means_df)
        print(n_snp_abs_means_df)
