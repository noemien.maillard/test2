# Creation date: 2023/04/24
# Last review: 2023/07/05
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Extract specified columns from enformer predictions output.

import logging
import pandas as pd


def reorder_cols(df: pd.DataFrame, firsts_cols: list) -> pd.DataFrame:
    """Reorder columns to get info columns at first, then experiment columns.

    Args:
        df (pd.DataFrame): Input dataframe.
        firsts_cols (list): Info columns to keep firsts.

    Returns:
        pd.DataFrame: Reordered dataframe with info columns firsts.
    """
    # store firsts_cols (and their content)
    new_df = df[firsts_cols]
    # drop firsts_cols from original dataframe
    df.drop(columns=firsts_cols, inplace=True)
    # concat info columns with experiments columns and return
    return pd.concat([new_df, df], axis=1)


def drop_pos(df: pd.DataFrame, filter_pos: list) -> pd.DataFrame:
    """Drop columns with positions to filter.

    Args:
        df (pd.DataFrame): Input dataframe.
        filter_pos (list): Positions to drop.

    Returns:
        pd.DataFrame: Dataframe with droped columns.
    """
    # drop positions (with ~ symbol) in POS column or write error in log file:
    try:
        return df[~df['POS'].isin(filter_pos)]
    except KeyError:
        logging.warn('Missed specified positions by using "POS" colname.')

    # fill log file and return df without droped positions
    logging.warn('All positions extracted.')
    return df


def keep_pos(df: pd.DataFrame, filter_pos: list):
    """Keep only specified positions.

    Args:
        df (pd.DataFrame): Input dataframe.
        filter_pos (list): Positions to keep.

    Returns:
        pd.DataFrame: Dataframe with kept columns.
    """
    # keep position in POS column or write error in log file
    try:
        return df[df['POS'].isin(filter_pos)]
    except KeyError:
        logging.warn('Missed specified positions by using "POS" colname.')

    # fill log file and return df with specified positions only
    logging.warn('All positions extracted.')
    return df


def drop_cols(df: pd.DataFrame, cols_to_drop: list, filter_pos: list, drop_positions: bool) -> pd.DataFrame:
    """Drop columns in cols_to_drop.

    Args:
        df (pd.DataFrame): Input dataframe.
        cols_to_drop (list): List containing names/patterns to drop in dataframe.

    Returns:
        pd.DataFrame: Dataframe without desired columns.
    """
    # drop each column containing pattern to find
    for condition in cols_to_drop:
        df = df.loc[:,~df.columns.str.contains(condition)]
        logging.info(condition)

    # keep or drop positions if filter_pos is not None (given drop_position value) and return final dataframe
    logging.info(f'Dataset size: {df.shape[0]} rows and {df.shape[1]} columns.')
    if filter_pos is None:
        return df
    elif filter_pos is not None and not drop_positions:
        return keep_pos(df=df, filter_pos=filter_pos)
    elif filter_pos is not None and drop_positions:
        return drop_pos(df=df, filter_pos=filter_pos)


def keep_cols(df: pd.DataFrame, cols_to_keep: list, filter_pos: list, drop_positions: bool) -> pd.DataFrame:
    """Keep only given columns.

    Args:
        df (pd.DataFrame): Input Dataframe.
        cols_to_keep (list): List containing names/patterns to keep in dataframe.

    Returns:
        pd.DataFrame: Dataframe with only desired columns.
    """
    # init dataframe to store and increment each column to keep
    new_df = pd.DataFrame()
    for condition in cols_to_keep:
        # filter df on column containing condition
        to_keep = df.loc[:,df.columns.str.contains(condition)]
        # increment new_df
        new_df = pd.concat([new_df, to_keep], axis=1)
        # drop columns added in new_df and log it
        df.drop(columns=to_keep.columns.to_list(), inplace=True)
        logging.info(condition)

    # keep or drop positions if filter_pos is not None (given drop_position value) and return final dataframe
    logging.info(f'Dataset size: {new_df.shape[0]} rows and {new_df.shape[1]} columns.')
    if filter_pos is None:
        return new_df
    elif filter_pos is not None and not drop_positions:
        return keep_pos(df=new_df, filter_pos=filter_pos)
    elif filter_pos is not None and drop_positions:
        return drop_pos(df=new_df, filter_pos=filter_pos)


def enformer(
        input: str,
        output: str,
        sep: str,
        index_col: bool,
        drop: list,
        drop_file: str,
        keep: list,
        keep_file: str,
        info_cols_file: str,
        info_cols_sep: str,
        filter_pos_file: str,
        drop_positions: bool
        ):
    """Main function which sets cols to drop/keep then run drop/keep function. If no output is precised, it is printed into terminal.

    Args:
        input (str): Input file containing table turned into pandas Dataframe.
        output (str): Output file to write from pandas Dataframe.
        sep (str): Separator used by pandas to turn input into Dataframe.
        index_col (bool): If true, first column of input is considered as index column else there is no index column.
        drop (list): List of columns names/patterns to drop. Useful when few columns to drop.
        drop_file (str): File containing columns names/patterns to drop. Useful when many columns to drop.
        keep (list): List of columns names/patterns to keep. Useful when few columns to keep.
        keep_file (str): File containing columns names/patterns to keep. Useful when many columns to keep.
        info_cols_file (str): File containing firsts columns with general informations (chrom, pos, id, ref, alt...).
        info_cols_sep (str): Separator of info_cols_file.
        filter_pos_file (str): File containing positions to filter.
        drop_positions (bool): If true, drop positions (in filter_pos_file) else keep them.
    """
    # read input table as pandas Dataframe with or without first column as index
    if index_col:
        df = pd.read_csv(input, sep=sep, engine='python', index_col=0)
    else:
        df = pd.read_csv(input, sep=sep, engine='python')
    # read info columns as pandas dataframe and check if same columns are used in df and info_cols
    # if different col names, change df col names
    info_cols = pd.read_csv(info_cols_file, sep=info_cols_sep, engine='python')
    if df.columns.to_list()[:5] != info_cols.columns.to_list():
        df.drop(columns=['chrom', 'pos', 'id', 'ref', 'alt'], inplace=True)
        logging.info(f'Input file firsts columns droped to replace by {info_cols.columns.to_list()}.')
        df = pd.concat([info_cols, df], axis=1)
    # store info cols columns (used for reordering)
    list_info_cols = info_cols.columns.to_list()

    # set cols_to_drop depending if names/patterns are given on the fly, in file or both
    if drop and drop_file is None:
        cols_to_drop=drop
    elif drop is None and drop_file:
        cols_to_drop = open(drop_file).read().splitlines()
    elif drop and drop_file:
        cols_to_drop = drop
        cols_to_drop.extend(open(drop_file).read().splitlines())
    # set cols_to_keep depending if names/patterns are given on the fly, in file or both
    # keep also info_cols
    elif keep and keep_file is None:
        cols_to_keep=keep
        cols_to_keep.extend(list_info_cols)
    elif keep is None and keep_file:
        cols_to_keep = open(keep_file).read().splitlines()
        cols_to_keep.extend(list_info_cols)
    elif keep and keep_file:
        cols_to_keep = open(keep_file).read().splitlines()
        cols_to_keep.extend(keep)
        cols_to_keep.extend(list_info_cols)
    
    # read filter pos in a list and turn into int
    if filter_pos_file is not None:
        filter_pos = open(filter_pos_file).read().splitlines()
        filter_pos = [int(pos) for pos in filter_pos]
    else:
        filter_pos = None
    
    # call drop or keep functions and output in file or terminal
    # generate output file
    if output:
        # drop columns
        if drop or drop_file:
            logging.info(f'Run mode: drop columns\nDrop options: {drop}\nDrop-file: {drop_file}\nColumns droped by following keywords/patterns')
            reorder_cols(df=drop_cols(df=df, cols_to_drop=cols_to_drop, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols).to_csv(path_or_buf=output, header=True, index=False)
        # keep columns
        elif keep or keep_file:
            logging.info(f'Run mode: keep columns\nKeep options: {keep}\nKeep-file: {keep_file}\nColumns kept by following keywords/patterns')
            reorder_cols(df=keep_cols(df=df, cols_to_keep=cols_to_keep, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols).to_csv(path_or_buf=output, header=True, index=False)
    # print in terminal
    else:
        # drop columns
        if drop or drop_file:
            print(reorder_cols(df=drop_cols(df=df, cols_to_drop=cols_to_drop, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols))
        # keep columns
        elif keep or keep_file:
            print(reorder_cols(df=keep_cols(df=df, cols_to_keep=cols_to_keep, filter_pos=filter_pos, drop_positions=drop_positions), firsts_cols=list_info_cols))
