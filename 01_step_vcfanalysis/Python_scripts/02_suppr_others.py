# Creation date: 2023/03/07
# Last review: 2023/06/28
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Remove individuals not in the PorQTL experimental design.
# IMPORTANT NOTE: This script has been designed for combined_individuals_bcmerged.vcf file specifically so columns are related to this one.

import argparse


def get_options() -> argparse.Namespace:
    """Set up and parse arguments.

    Returns:
        Namespace: Parser with arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'i',
        help="Name of input file"
    )
    parser.add_argument(
        'o',
        help="Name of output file"
    )
    return parser.parse_args()


def suppr_cols(input: str, output: str):
    """Take off individuals which are not in the PorQTL experimental design and rewrite new file with PorQTL individuals only.
    The original file is kept.

    Args:
        input (str): Input vcf file name.
        output (str): Output vcf file name.
    """
    with open(input) as ipf:
        with open(output, 'w') as opf:
            for line in ipf:
                if line.startswith('#'):
                    # past firsts lines with parameters from previous algorithms runs
                    if line.startswith('##'):
                        opf.write(line)
                    else:
                        # extract columns of individuals (headers) in the PorQTL experimental design only
                        line = line.split('\t')
                        # cols 23, 24, 29 and 37 to the last one are "others"
                        new_headers = line[0: 23]
                        new_headers.extend(line[25:29])
                        new_headers.extend(line[30:37])
                        new_headers = '\t'.join(new_headers)
                        opf.write(f'{new_headers}\n')
                else:
                    # extract columns of individuals (data informations) in the PorQTL experimental design only
                    line = line.split('\t')
                    # cols 23, 24, 29 and 37 to the last one are "others"
                    new_line = line[0: 23]
                    new_line.extend(line[25:29])
                    new_line.extend(line[30:37])
                    new_line = '\t'.join(new_line)
                    opf.write(f'{new_line}\n')
                

def main():
    args = get_options()
    suppr_cols(input=args.i, output=args.o)


if __name__ == '__main__':
    main()
