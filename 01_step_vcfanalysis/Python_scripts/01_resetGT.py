# Creation date: 2023/02/22
# Last review: 2023/06/28
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Change genotypes into ./. to prepare imputation
# Run: python .\resetGT.py -i .\combined_individuals.vcf -o .\combined_individuals_bcmerged.vcf -p .\positions_bc1_bc2.txt -r .\primers_whole_dataset.txt

import argparse


def get_options():
    """Set up and parse arguments.

    Returns:
        Namespace: Parser with arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--input',
        help="Input file. Can include path."
    )
    parser.add_argument(
        '-o',
        '--output',
        help="Output file. Can include path."
    )
    parser.add_argument(
        '-p',
        '--positions',
        help="File containing positions to check where genotypes are different."
    )
    parser.add_argument(
        '-r',
        '--primers-exceptions',
        action='store',
        help="File containing positions specific to primers amplicons."
    )
    return parser.parse_args()


def isinpos(bc_wgs: list, bc_amp: list) -> bool:
    """Check, for the same individual, if wgs OR amplicons genotypes is unknown OR if genotypes are different.
    If so return true, else false.
    So False means both genotypes are unknown or they are known but same.

    Args:
        bc_wgs (list): back-cross list of informations from wgs genotyping. First element is the genotype.
        bc_amp (list): back-cross list of informations from amplicons genotyping. First element is the genotype.

    Returns:
        bool: True if genotypes are different or one is unknown, else False.
    """
    if bc_wgs[0] in ['./.', '.|.'] and bc_amp[0] not in ['./.', '.|.']:
        return True
    elif bc_wgs[0] not in ['./.', '.|.'] and bc_amp[0] in ['./.', '.|.']:
        return True
    elif bc_wgs[0] != bc_amp[0]:
        return True
    return False


def write_file(input_file: str, output_file: str, positions_file: str=None, primers_exceptions: str=None):
    """Write file with corrected genotypes for back-cross genotyped in wgs and amplicons.
    Erase 2 last columns (=back-cross genotyped with amplicons strategy) in output file and merge back-cross (keep input file intact).
    Conditions are dependant on positions, if they are in positions_file and if they are in primers_exceptions.
    For both back-cross, if genotypes (gt) are same, keep it. If position is in "positions_file" and not in "primers_exceptions"
    check depth in wgs and amp and keep gt if it is greater than threshold in one or the other one, else reset gt. If position is in
    "positions_file" and "primers_exceptions", reset all amp individuals and keep wgs if greater than threshold, else reset gt.

    Args:
        input_file (str): Input vcf file name.
        output_file (str): Output vcf file name.
        positions_file (str, optional): File containing positions to modify. Defaults to None.
        primers_exceptions (str, optional): File containing positions in problematic primers couples. Defaults to None.
    """
    # store positions to modify
    if positions_file is not None:
        pos = open(positions_file).readlines()
        pos = [int(position) for position in pos]
    # store positions specific to primers
    if primers_exceptions is not None:
        primers_pos = open(primers_exceptions).readlines()
        primers_pos = [int(prim) for prim in primers_pos]
    # write file
    with open(input_file) as inf:
        with open(output_file, 'w') as outf:
            for line in inf:
                if line.startswith('#'):
                    # write lines from previous analysis (GATK, etc)
                    if line.startswith('##'):
                        outf.write(line)
                    else:
                        # write headers
                        headers = "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t17M20890690\t17M20890738\t17M20890755\t17M20890761\t17M20890768\t17M20890769\t17M30880607\t17M30880608\t17M30900789\t17M30900839\t17M30900848\t17M30900849\tFR17MAG201003217\tFR18GAL200900504\t10PIG03R3134\t17MAG986765\t17MAG991373\t18GAL010179\t18GAL010180\t18GAL010332\t18GAL030759\t18GAL031357\t18GAL042551\t18GAL042595\t18GAL050012\t18GAL050014\t18GAL050144\t18GAL050163\t35FWQ03H4417\t35FWQ05K0465\t44DER030686\t49QHN014662"
                        outf.write(f"{headers}\n")
                    continue
                variant = line.split('\t')
                var_pos = int(variant[1])  # variant position
                # if variant is not in pos of interest (pos to change)
                if var_pos not in pos:
                    # if variant don't have to be changed, keep WGS information
                    if var_pos not in primers_pos:
                        new_line = variant[:41]
                        new_line = '\t'.join(new_line)
                        outf.write(f"{new_line}\n")
                    # elif variant in primers, keep WGS and reset amp for all individuals genotyped in amp only
                    elif var_pos in primers_pos:
                        # we cannot trust "primers" positions in amp
                        new_line = variant[:6]
                        new_line.append("RO")  # RO = reset others
                        new_line.extend(variant[7:23])
                        # if my pos is in primers, all amp are reset to ./.
                        others_to_split = variant[23:41]
                        for i, other in enumerate(others_to_split):
                            indiv = other.split(':')
                            new_indiv = ['./.']
                            new_indiv.extend(indiv[1:])
                            indiv = ':'.join(new_indiv)
                            others_to_split[i] = indiv
                        new_line.extend(others_to_split)
                        new_line = '\t'.join(new_line)
                        outf.write(f"{new_line}\n")
                # if variant is in pos of interest (pos to change)
                else:
                    # set variants genotypes
                    # bc[1] = GT, bc[2] = AD, etc (cf FORMAT column)
                    bc1_wgs = variant[21].split(':')
                    bc1_amp = variant[41].split(':')
                    bc2_wgs = variant[22].split(':')
                    bc2_amp = variant[42].strip('\n').split(':')  # bc2_amp is the last col of the line so need to strip last char
                    bc1 = "bc1"
                    bc2 = "bc2"
                    others = None
                    nogt = ['./.', '.|.']
                    if var_pos in primers_pos:
                        # bc1 in pos, not bc2
                        if isinpos(bc1_wgs, bc1_amp) is True and isinpos(bc2_wgs, bc2_amp) is False:
                            # if wgs has GT (and not amp)
                            if bc1_wgs[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.']) > 14:
                                    bc1 = ':'.join(bc1_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            # if amp has GT (and not wgs) reset GT to ./. because it is in primers pos (problematic amplicons)
                            else:
                                new_bc1 = ['./.']
                                new_bc1.extend(bc1_amp[1:])
                                bc1 = ':'.join(new_bc1)
                            # bc2 is not in primers_pos so I keep wgs column
                            bc2 = ':'.join(bc2_wgs)
                        # bc1 not in pos, bc2 in pos
                        elif isinpos(bc1_wgs, bc1_amp) is False and isinpos(bc2_wgs, bc2_amp) is True:
                            # if wgs has GT (and not amp)
                            if bc2_wgs[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.']) > 14:
                                    bc2 = ':'.join(bc2_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if amp has GT (and not wgs) reset GT to ./. because it is in primers pos (problematic amplicons)
                            else:
                                new_bc2 = ['./.']
                                new_bc2.extend(bc2_amp[1:])
                                bc2 = ":".join(new_bc2)
                            # bc1 is not in primers_pos so I keep wgs column
                            bc1 = ':'.join(bc1_wgs)
                        # bc1 and bc2 are in pos
                        elif isinpos(bc1_wgs, bc1_amp) is True and isinpos(bc2_wgs, bc2_amp) is True:
                            # BC1
                            # if bc1_wgs has GT (and no bc1_amp)
                            if bc1_wgs[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.']) > 14:
                                    bc1 = ':'.join(bc1_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            # if bc1_amp has GT (and not bc1_wgs) reset GT to ./. because it is in primers pos (problematic amplicons)
                            else:
                                new_bc1 = ['./.']
                                new_bc1.extend(bc1_amp[1:])
                                bc1 = ':'.join(new_bc1)
                            # BC2
                            # if bc2_wgs has GT (and no bc2_amp)
                            if bc2_wgs[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.']) > 14:
                                    bc2 = ":".join(bc2_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if bc2_amp has GT (and not bc2_wgs) reset GT to ./. because it is in primers pos (problematic amplicons)
                            else:
                                new_bc2 = ['./.']
                                new_bc2.extend(bc2_amp[1:])
                                bc2 = ':'.join(new_bc2)
                        # if my pos is in primers, all amp are reset to ./.
                        others_to_split = variant[23:41]
                        for i, other in enumerate(others_to_split):
                            indiv = other.split(':')
                            new_indiv = ['./.']
                            new_indiv.extend(indiv[1:])
                            indiv = ':'.join(new_indiv)
                            others_to_split[i] = indiv
                        others = others_to_split   
                    # if variant not in primers but in pos to change
                    else:
                        # bc1 in pos, not bc2
                        if isinpos(bc1_wgs, bc1_amp) is True and isinpos(bc2_wgs, bc2_amp) is False:
                            # if wgs has GT (and not amp)
                            if bc1_wgs[0] not in nogt and bc1_amp[0] in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.']) > 14:
                                    bc1 = ':'.join(bc1_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            # if amp has GT (and not wgs)
                            elif bc1_wgs[0] in nogt and bc1_amp[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_amp[1].split(',') if depth !='.']) > 29:
                                    bc1 = ':'.join(bc1_amp)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_amp[1:])
                                    bc1 = ':'.join(new_bc1)
                            # if WGS and amp have GT, check depth
                            elif bc1_wgs[0] not in nogt and bc1_amp[0] not in nogt:
                                depth_wgs = sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.'])
                                depth_amp = sum([int(depth) for depth in bc1_amp[1].split(',') if depth !='.'])
                                # if WGS depth pass threshold and not amp depth, keep WGS GT
                                if depth_wgs > 14 and depth_amp < 30:
                                    bc1 = ':'.join(bc1_wgs)
                                # if WGS depth does not pass threshold and amp depth does, keep amp GT
                                elif depth_wgs < 15 and depth_amp > 29:
                                    bc1 = ':'.join(bc1_amp)
                                # if WGS depth pass threshold and amp depth also does pass threshold, reset GT
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            bc2 = ':'.join(bc2_wgs)
                        # bc1 not in pos, bc2 in pos
                        elif isinpos(bc1_wgs, bc1_amp) is False and isinpos(bc2_wgs, bc2_amp) is True:
                            # if wgs has GT (and not amp)
                            if bc2_wgs[0] not in nogt and bc2_amp[0] in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.']) > 14:
                                    bc2 = ':'.join(bc2_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if amp has GT (and not wgs)
                            elif bc2_wgs[0] in nogt and bc2_amp[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_amp[1].split(',') if depth !='.']) > 29:
                                    bc2 = ':'.join(bc2_amp)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_amp[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if WGS and amp have GT, check depth
                            elif bc2_wgs[0] not in nogt and bc2_amp[0] not in nogt:
                                depth_wgs = sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.'])
                                depth_amp = sum([int(depth) for depth in bc2_amp[1].split(',') if depth !='.'])
                                # if WGS depth pass threshold and not amp depth, keep WGS GT
                                if depth_wgs > 14 and depth_amp < 30:
                                    bc2 = ':'.join(bc2_wgs)
                                # if WGS depth does not pass threshold and amp depth does, keep amp GT
                                elif depth_wgs < 15 and depth_amp > 29:
                                    bc2 = ':'.join(bc2_amp)
                                # if WGS depth pass threshold and amp depth also does pass threshold, reset GT
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                            bc1 = ':'.join(bc1_wgs)
                        # both bc1 and bc2 are in pos
                        elif isinpos(bc1_wgs, bc1_amp) is True and isinpos(bc2_wgs, bc2_amp) is True:
                            # BC1
                            # if wgs has GT (and not amp)
                            if bc1_wgs[0] not in nogt and bc1_amp[0] in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.']) > 14:
                                    bc1 = ':'.join(bc1_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            elif bc1_wgs[0] in nogt and bc1_amp[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc1_amp[1].split(',') if depth !='.']) > 29:
                                    bc1 = ':'.join(bc1_amp)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_amp[1:])
                                    bc1 = ':'.join(new_bc1)
                            # if WGS and amp have GT, check depth
                            elif bc1_wgs[0] not in nogt and bc1_amp[0] not in nogt:
                                depth_wgs = sum([int(depth) for depth in bc1_wgs[1].split(',') if depth !='.'])
                                depth_amp = sum([int(depth) for depth in bc1_amp[1].split(',') if depth !='.'])
                                # if WGS depth pass threshold and not amp depth, keep WGS GT
                                if depth_wgs > 14 and depth_amp < 30:
                                    bc1 = ':'.join(bc1_wgs)
                                # if WGS depth does not pass threshold and amp depth does, keep amp GT
                                elif depth_wgs < 15 and depth_amp > 29:
                                    bc1 = ':'.join(bc1_amp)
                                # if WGS depth pass threshold and amp depth also does pass threshold, reset GT
                                else:
                                    new_bc1 = ['./.']
                                    new_bc1.extend(bc1_wgs[1:])
                                    bc1 = ':'.join(new_bc1)
                            # BC2
                            # if wgs has GT (and not amp)
                            if bc2_wgs[0] not in nogt and bc2_amp[0] in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.']) > 14:
                                    bc2 = ':'.join(bc2_wgs)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if amp has no GT (and not wgs)
                            elif bc2_wgs[0] in nogt and bc2_amp[0] not in nogt:
                                # if depth is greater than threshold, just keep GT
                                if sum([int(depth) for depth in bc2_amp[1].split(',') if depth !='.']) > 29:
                                    bc2 = ':'.join(bc2_amp)
                                # if depth is lower than threshold, change GT to ./.
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_amp[1:])
                                    bc2 = ':'.join(new_bc2)
                            # if WGS and amp have GT, check depth
                            elif bc2_wgs[0] not in nogt and bc2_amp[0] not in nogt:
                                depth_wgs = sum([int(depth) for depth in bc2_wgs[1].split(',') if depth !='.'])
                                depth_amp = sum([int(depth) for depth in bc2_amp[1].split(',') if depth !='.'])
                                # if WGS depth pass threshold and not amp depth, keep WGS GT
                                if depth_wgs > 14 and depth_amp < 30:
                                    bc2 = ':'.join(bc2_wgs)
                                # if WGS depth does not pass threshold and amp depth does, keep amp GT
                                elif depth_wgs < 15 and depth_amp > 29:
                                    bc2 = ':'.join(bc2_amp)
                                # if WGS depth pass threshold and amp depth also does pass threshold, reset GT
                                else:
                                    new_bc2 = ['./.']
                                    new_bc2.extend(bc2_wgs[1:])
                                    bc2 = ':'.join(new_bc2)
                    # concatenate and write line
                    new_line = variant[:6]
                    # if others is not None (meaning others have been changed so positions are in primers)
                    if others is not None:
                        new_line.append("MBRO")  # MBRO = merge bc and reset others (amplicons in primers)
                        new_line.extend(variant[7:21])
                        new_line.extend([bc1, bc2])
                        new_line.extend(others)
                        new_line = '\t'.join(new_line)
                        outf.write(f"{new_line}\n")
                    # if others is None (meaning others have not been changed so positions are not in primers)
                    else:
                        new_line.append("MB")  # MB = merge bc
                        new_line.extend(variant[7:21])
                        new_line.extend([bc1, bc2])
                        new_line.extend(variant[23:41])
                        new_line = '\t'.join(new_line)
                        outf.write(f"{new_line}\n")


def main():
    args = get_options()
    write_file(input_file=args.input, output_file=args.output, positions_file=args.positions, primers_exceptions=args.primers_exceptions)

if __name__ == '__main__':
    main()