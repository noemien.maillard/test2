# Creation date: 2023/02/07
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: QTL1_porc (M2R)
# Script aim: Compare BC from initial dataset (with 12 founders) and amplicons dataset (with 20 individuals)
# Modules versions: R version 4.2.1, dplyr 1.1.0, ggplot2 3.4.2, ggpub 0.6.0, reshape2 1.4.4, scales 1.2.1, stringr 1.5.0, tidyr 1.3.0

library(dplyr)
library(ggplot2)
library(ggpubr)
library(reshape2)
library(scales)
library(stringr)
library(tidyr)

setwd('C:/Users/nomaillard/Documents/')

# ggplot theme setup
theme_update(plot.title = element_text(hjust = 0.5))  # to set centered title for all plots
theme_update(plot.subtitle = element_text(hjust = 0.5))  # to set centered subtitle for all plots
theme_update(panel.background=element_rect(fill="white", color='black'))
theme_update(panel.grid=element_blank())

# load dataset and look its caracteristics
data <- read.csv('combined_individuals_table.txt', header=T, sep='\t')

head(data)
length(unique(data$POS))  # 7654

# take out informations columns
info_cols <- select(data, c(1:9))
head(info_cols)
# take out back-cross 1 (bc1 = FR17MAG201003217) from both originals datasets columns
bc1 <- data[c(22, 42)]
head(bc1)
# bind informations columns with individuals columns
bc1 <- bind_cols(info_cols, bc1)
head(bc1)
nrow(bc1)  # 7682

# primers
primers <- read.csv("primers.txt", header=F, sep='\t')
colnames(primers) <- c('name', 'start', 'stop')

#########################################
##########     bc1 dataset     ##########
#########################################

# extract GT and AD from individuals (this rename the original column and create a second column for AD)
# Note: warning message is normal, we are interested only on the 2 firsts fields related to FORMAT col.
bc1 <- bc1 %>% separate(FR17MAG201003217, c('WGS_GT', 'WGS_AD'), sep=':')
bc1$WGS_GT
bc1$WGS_AD
bc1 <- bc1 %>% separate(X2.FR17MAG201003217, c('amp_GT', 'amp_AD'), sep=":")
bc1$amp_GT
bc1$amp_AD
colnames(bc1)

# split AD values with "," separator
bc1$WGS_AD <- strsplit(bc1$WGS_AD, split=",")
bc1$amp_AD <- strsplit(bc1$amp_AD, split=",")

##### separate data depending on genotypes (GT)
# df with WGS_GT == "./." and amp_GT == "./."
bc1_noGT <- bc1[bc1$WGS_GT == "./." & bc1$amp_GT == "./.",]
nrow(bc1_noGT)  # 861 / 860 unique positions
# df with WGS_GT == "./." and amp_GT != "./."
bc1_WGS_noGT <- bc1[bc1$WGS_GT == "./." & bc1$amp_GT != "./.",]
nrow(bc1_WGS_noGT)  # 1769 / 1769 unique positions
# df with WGS_GT != "./." and amp_GT == "./."
bc1_amp_noGT <- bc1[bc1$WGS_GT != "./." & bc1$amp_GT == "./.",]
nrow(bc1_amp_noGT)  # 570 / 570 unique positions
# df with WGS_GT != "./." and amp_GT != "./."
bc1_allGT <- bc1[bc1$WGS_GT != "./." & bc1$amp_GT != "./.",]
nrow(bc1_allGT)  # 4482 / 4482 unique positions
# df with WGS_GT != amp_GT (excluding "./." in both cols)
bc1_allGT_GTdiff <- bc1_allGT[bc1_allGT$WGS_GT != bc1_allGT$amp_GT,]
nrow(bc1_allGT_GTdiff)  # 423 / 423 unique positions

# store positions in more than 1 line
double_line <- c()
for (pos in unique(bc1$POS)){if (nrow(bc1[bc1$POS==pos,])>1){double_line <- c(double_line, pos)}}

# plot input dataset
bc1_infoGT <- data.frame()
# indels and doubled positions which will be set to ./. to avoid conflicts
indels_doubled <- c(intersect(bc1_noGT$POS, bc1_WGS_noGT$POS), intersect(bc1_amp_noGT$POS, bc1_WGS_noGT$POS), intersect(bc1_noGT$POS, bc1_amp_noGT$POS))
for (pos in unique(bc1$POS)){
  if (pos %in% double_line){
    bc1_infoGT <- rbind(bc1_infoGT, data.frame(
      position = pos,
      infoGT = 'noGT',
      DPWGS = 0,
      DPamp = 0))
    next
  }
  if (pos %in% bc1_noGT$POS){
    infoGT <- 'noGT'
  }else if(pos %in% bc1_WGS_noGT$POS){
    infoGT <- 'GT_amp_only'
  }else if(pos %in% bc1_amp_noGT$POS){
    infoGT <- 'GT_WGS_only'
  }else if(pos %in% bc1_allGT_GTdiff$POS){
    infoGT <- 'GTdiff'
  }else{
    infoGT <- 'sameGT'
  }
  bc1_infoGT <- rbind(bc1_infoGT, data.frame(
    position = pos,
    infoGT = infoGT,
    DPWGS = sum(as.numeric(unlist(bc1$WGS_AD[bc1$POS==pos][! bc1$WGS_AD[bc1$POS==pos] %in% c('.')]))),
    DPamp = sum(as.numeric(unlist(bc1$amp_AD[bc1$POS==pos][! bc1$amp_AD[bc1$POS==pos] %in% c('.')])))
  ))
}
head(bc1_infoGT)
summary(bc1_infoGT)

# count number of positions in each subsets
nrow(bc1_infoGT[bc1_infoGT$infoGT=='noGT',])  # 880
nrow(bc1_infoGT[bc1_infoGT$infoGT=='sameGT',])  # 4059
nrow(bc1_infoGT[bc1_infoGT$infoGT=='GT_amp_only',])  # 1743
nrow(bc1_infoGT[bc1_infoGT$infoGT=='GT_WGS_only',])  # 549
nrow(bc1_infoGT[bc1_infoGT$infoGT=='GTdiff',])  # 423

# global
png(filename='starting_dataset.png', width=700, height=500)
ggplot(bc1_infoGT) +
  geom_point(aes(x=DPWGS, y=DPamp, color=infoGT, alpha=0.7)) +
  scale_color_manual(values=c('sameGT'='#a6edeb', 'noGT'='#d97af2', 'GT_WGS_only'='#1d8022', 'GTdiff'='#f66262', 'GT_amp_only'='#dfb911')) +
  xlim(0, 45) +
  ylim(0, 650) +
  theme(axis.title=element_blank(), axis.text=element_text(size=15))
dev.off()
# same as above but with bigger legend (for figure)
png(filename='legend_starting_dataset.png', width=700, height=500)
ggplot(bc1_infoGT) +
  geom_point(aes(x=DPWGS, y=DPamp, color=infoGT, alpha=0.7)) +
  scale_color_manual(values=c('sameGT'='#a6edeb', 'noGT'='#d97af2', 'GT_WGS_only'='#1d8022', 'GTdiff'='#f66262', 'GT_amp_only'='#dfb911')) +
  xlim(0, 45) +
  ylim(0, 650) +
  guides(colour = guide_legend(override.aes = list(size=5))) +
  theme(axis.title=element_blank(), axis.text=element_text(size=15), legend.text=element_text(size=15), legend.title=element_text(size=18))
dev.off()

# compare GT
bc1_allGT[,c('WGS_GT', 'amp_GT')]
bc1_allGT_GTdiff[,c('WGS_GT', 'amp_GT')]

##### analysis of data without information on GT in WGS only (1769 variants)
# fill DP column with depth which is the sum of depth of each allele (do it for both individuals: WGS and ampli (amp))
# Note: .=0 so "." are removed ( => [! bc1_WGS_noGT$WGS_AD[[i]] %in% c('.')] )
# Note 1/2: Because WGS have no GT, this step is useless but I chose to keep the same steps all over the script
for (i in 1:nrow(bc1_WGS_noGT)){
  bc1_WGS_noGT$WGS_DP[[i]] <- sum(as.numeric(unlist(bc1_WGS_noGT$WGS_AD[[i]][! bc1_WGS_noGT$WGS_AD[[i]] %in% c('.')])))
}
# to print columns (optional)
for (i in 1:nrow(bc1_WGS_noGT)){
  if (bc1_WGS_noGT$WGS_DP[[i]] != 0){
    print(i)
  }
}
# unlist returns a vector we can work with
bc1_WGS_noGT$WGS_DP <- unlist(bc1_WGS_noGT$WGS_DP)

# Note 2/2: Only this step is useful but I chose to keep the same steps all over the script
for (i in 1:nrow(bc1_WGS_noGT)){
  bc1_WGS_noGT$amp_DP[[i]] <- sum(as.numeric(unlist(bc1_WGS_noGT$amp_AD[[i]][! bc1_WGS_noGT$amp_AD[[i]] %in% c('.')])))
}
for (i in 1:nrow(bc1_WGS_noGT)){
  if (bc1_WGS_noGT$amp_DP[[i]] != 0){
    print(i)
  }
}
bc1_WGS_noGT$amp_DP <- unlist(bc1_WGS_noGT$amp_DP)

#reorder cols
colnames(bc1_WGS_noGT)
bc1_WGS_noGT <- bc1_WGS_noGT[, c(1:11, 14, 12, 13, 15)]
colnames(bc1_WGS_noGT)
head(bc1_WGS_noGT)
print(paste('data rows:', nrow(bc1_WGS_noGT)))
print(paste('WGS DP with depth:', nrow(bc1_WGS_noGT[bc1_WGS_noGT$WGS_DP != 0,])))
print(paste('amp DP with depth:', nrow(bc1_WGS_noGT[bc1_WGS_noGT$amp_DP != 0,])))

# verification no loss of data
if (nrow(bc1) == sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))){
  print(paste('No loss of data, nrow =', nrow(bc1)))
}else{
  print('!!! DATA LOST !!!')
  print(paste('nrow(bc1) =', nrow(bc1)))
  print(paste('sum(datasets) =', sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))))
}

##### plots
# set color
lightblue='#4a44ef'
# amp count table (n=count)
bc1_WGS_noGT_amp_depth <- data.frame(bc1_WGS_noGT$amp_DP)
bc1_WGS_noGT_amp_depth <- bc1_WGS_noGT_amp_depth %>%
  group_by_all() %>%
  count()
colnames(bc1_WGS_noGT_amp_depth)
colnames(bc1_WGS_noGT_amp_depth) <- c('amp_DP', 'count')
head(bc1_WGS_noGT_amp_depth)

# create an empty table from 0 to max(amp_DP)
bc1_max_ampDP = max(bc1_WGS_noGT_amp_depth$amp_DP)  # max_raw = 683
bc1amp_DPcount = data.frame(matrix(0, nrow = bc1_max_ampDP+1, ncol = 2))
head(bc1amp_DPcount)
colnames(bc1amp_DPcount) <- c('depth', 'amp_count')
bc1amp_DPcount$depth <- c(0:bc1_max_ampDP)
head(bc1amp_DPcount)
tail(bc1amp_DPcount)

# fill table
for (i in 0:nrow(bc1amp_DPcount)-1){
  bc1amp_DPcount[which(bc1amp_DPcount$depth==i),]['amp_count'] <- bc1_WGS_noGT_amp_depth[which(bc1_WGS_noGT_amp_depth$amp_DP==i),]['count']
}
# check the table is correctly filled with values (from start to end)
head(bc1amp_DPcount)
tail(bc1amp_DPcount)
bc1amp_DPcount[is.na(bc1amp_DPcount)] = 0
head(bc1amp_DPcount)
tail(bc1amp_DPcount)
nrow(bc1amp_DPcount)

png(filename="bc1_DPamp_notitle.png", width=700, height=500)
ggplot(bc1amp_DPcount, aes(x=depth, y=amp_count)) +
  geom_bar(stat = "summary", fill=lightblue, width=1) +
  geom_vline(xintercept=29.5, linetype="dashed", linewidth=0.5) +
  xlim(0, 300) +
  theme(legend.position="none", axis.title=element_blank(), axis.text=element_text(size=15)) +
  labs(x='Variant depth in amp', y='Number of variants')
dev.off()

##### analysis of data with information on GT in ampli only (570 variants)
# fill DP column with depth which is the sum of depth of each allele (do it for both individuals: WGS and ampli (amp))
# Note 1/2: Only this step is useful but I chose to keep the same steps all over the script
for (i in 1:nrow(bc1_amp_noGT)){
  bc1_amp_noGT$WGS_DP[[i]] <- sum(as.numeric(unlist(bc1_amp_noGT$WGS_AD[[i]][! bc1_amp_noGT$WGS_AD[[i]] %in% c('.')])))
}
# to print columns (optional)
for (i in 1:nrow(bc1_amp_noGT)){
  if (bc1_amp_noGT$WGS_DP[[i]] != 0){
    print(i)
  }
}
# unlist returns a vector we can work with
bc1_amp_noGT$WGS_DP <- unlist(bc1_amp_noGT$WGS_DP)

# Note 2/2: Because amp have no GT, this step is useless but I chose to keep the same steps all over the script
for (i in 1:nrow(bc1_amp_noGT)){
  bc1_amp_noGT$amp_DP[[i]] <- sum(as.numeric(unlist(bc1_amp_noGT$amp_AD[[i]][! bc1_amp_noGT$amp_AD[[i]] %in% c('.')])))
}
for (i in 1:nrow(bc1_amp_noGT)){
  if (bc1_amp_noGT$amp_DP[[i]] != 0){
    print(i)
  }
}
bc1_amp_noGT$amp_DP <- unlist(bc1_amp_noGT$amp_DP)

#reorder cols
colnames(bc1_amp_noGT)
bc1_amp_noGT <- bc1_amp_noGT[, c(1:11, 14, 12, 13, 15)]
colnames(bc1_amp_noGT)
head(bc1_amp_noGT)
print(paste('data rows:', nrow(bc1_amp_noGT)))
print(paste('WGS DP != 0:', nrow(bc1_amp_noGT[bc1_amp_noGT$WGS_DP != 0,])))
print(paste('amp DP != 0:', nrow(bc1_amp_noGT[bc1_amp_noGT$amp_DP != 0,])))

# verification no loss of data
if (nrow(bc1) == sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))){
  print(paste('No loss of data, nrow =', nrow(bc1)))
}else{
  print('!!! DATA LOST !!!')
  print(paste('nrow(bc1) =', nrow(bc1)))
  print(paste('sum(datasets) =', sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))))
}

##### plots
# set color
lightred = "#f54c76"
# WGS count table (n=count)
bc1_amp_noGT_WGS_depth <- data.frame(bc1_amp_noGT$WGS_DP)
bc1_amp_noGT_WGS_depth <- bc1_amp_noGT_WGS_depth %>%
  group_by_all() %>%
  count()
colnames(bc1_amp_noGT_WGS_depth)
colnames(bc1_amp_noGT_WGS_depth) <- c('WGS_DP', 'count')
head(bc1_amp_noGT_WGS_depth)

# create an empty table from 0 to max(WGS_DP)
bc1_max_WGSDP = max(bc1_amp_noGT_WGS_depth$WGS_DP)  # max_raw = 33
bc1_WGS_DPcount = data.frame(matrix(0, nrow = bc1_max_WGSDP+1, ncol = 2))
head(bc1_WGS_DPcount)
colnames(bc1_WGS_DPcount) <- c('depth', 'WGS_count')
bc1_WGS_DPcount$depth <- c(0:bc1_max_WGSDP)
head(bc1_WGS_DPcount)
tail(bc1_WGS_DPcount)

# fill table
for (i in 0:nrow(bc1_WGS_DPcount)-1){
  bc1_WGS_DPcount[which(bc1_WGS_DPcount$depth==i),]['WGS_count'] <- bc1_amp_noGT_WGS_depth[which(bc1_amp_noGT_WGS_depth$WGS_DP==i),]['count']
}
# check the table is correctly filled with values (from start to end)
head(bc1_WGS_DPcount)
tail(bc1_WGS_DPcount)
bc1_WGS_DPcount[is.na(bc1_WGS_DPcount)] = 0
head(bc1_WGS_DPcount)
tail(bc1_WGS_DPcount)

png(filename="bc1_DPWGS_notitle.png", width=700, height=500)
ggplot(bc1_WGS_DPcount, aes(x=depth, y=WGS_count)) +
  geom_bar(stat='summary', fill=lightred) +
  geom_vline(xintercept=14.5, linetype="dashed", linewidth=0.5) +
  theme(legend.position="none", axis.title=element_blank(), axis.text=element_text(size=15)) +
  labs(x='Variant depth in WGS', y='Number of variants')
dev.off()


##### analysis of data with information on GT in GWS and ampli (423 variants)
# fill DP column with depth which is the sum of depth of each allele (do it for both individuals: WGS and ampli (amp))
# Note: In this case, WGS and amp have GT so both depths are useful
for (i in 1:nrow(bc1_allGT_GTdiff)){
  bc1_allGT_GTdiff$WGS_DP[[i]] <- sum(as.numeric(unlist(bc1_allGT_GTdiff$WGS_AD[[i]][! bc1_allGT_GTdiff$WGS_AD[[i]] %in% c('.')])))
}
# to print columns (optional)
for (i in 1:nrow(bc1_allGT_GTdiff)){
  if (bc1_allGT_GTdiff$WGS_DP[[i]] != 0){
    print(i)
  }
}
# unlist returns a vector we can work with
bc1_allGT_GTdiff$WGS_DP <- unlist(bc1_allGT_GTdiff$WGS_DP)

for (i in 1:nrow(bc1_allGT_GTdiff)){
  bc1_allGT_GTdiff$amp_DP[[i]] <- sum(as.numeric(unlist(bc1_allGT_GTdiff$amp_AD[[i]][! bc1_allGT_GTdiff$amp_AD[[i]] %in% c('.')])))
}
for (i in 1:nrow(bc1_allGT_GTdiff)){
  if (bc1_allGT_GTdiff$amp_DP[[i]] != 0){
    print(i)
  }
}
bc1_allGT_GTdiff$amp_DP <- unlist(bc1_allGT_GTdiff$amp_DP)

#reorder cols
colnames(bc1_allGT_GTdiff)
bc1_allGT_GTdiff <- bc1_allGT_GTdiff[, c(1:11, 14, 12, 13, 15)]
colnames(bc1_allGT_GTdiff)
head(bc1_allGT_GTdiff)
print(paste('data rows:', nrow(bc1_allGT_GTdiff)))
print(paste('WGS DP != 0:', nrow(bc1_allGT_GTdiff[bc1_allGT_GTdiff$WGS_DP != 0,])))
print(paste('amp DP != 0:', nrow(bc1_allGT_GTdiff[bc1_allGT_GTdiff$amp_DP != 0,])))

# verification no loss of data
if (nrow(bc1) == sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))){
  print(paste('No loss of data, nrow =', nrow(bc1)))
}else{
  print('!!! DATA LOST !!!')
  print(paste('nrow(bc1) =', nrow(bc1)))
  print(paste('sum(datasets) =', sum(nrow(bc1_noGT), nrow(bc1_WGS_noGT), nrow(bc1_amp_noGT), nrow(bc1_allGT))))
}

##### plots
# WGS counts table
bc1_allGT_GTdiff_WGS_depth <- data.frame(bc1_allGT_GTdiff$WGS_DP)
bc1_allGT_GTdiff_WGS_depth <- bc1_allGT_GTdiff_WGS_depth %>%
  group_by_all() %>%
  count()
colnames(bc1_allGT_GTdiff_WGS_depth)
colnames(bc1_allGT_GTdiff_WGS_depth) <- c('WGS_DP', 'n')  # change col names
# amp counts table
bc1_allGT_GTdiff_amp_depth <- data.frame(bc1_allGT_GTdiff$amp_DP)
bc1_allGT_GTdiff_amp_depth <- bc1_allGT_GTdiff_amp_depth %>%
  group_by_all() %>%
  count()
colnames(bc1_allGT_GTdiff_amp_depth)
colnames(bc1_allGT_GTdiff_amp_depth) <- c('amp_DP', 'n')  # change col names
tail(bc1_allGT_GTdiff_amp_depth, n=20)

### create a table to "bind" WGS and amp depths counts
# init table
# I took values from step before table building
bc1_max_raw = max(bc1_allGT_GTdiff$WGS_DP, bc1_allGT_GTdiff$amp_DP)  # max_raw = 492
bc1_allGT_GTdiff_DPcount = data.frame(matrix(0, nrow = bc1_max_raw+1, ncol = 3))
# change col names
colnames(bc1_allGT_GTdiff_DPcount) <- c('depth', 'WGS_DP_count', 'amp_DP_count')
# change depth values to range from 0 to max (492)
bc1_allGT_GTdiff_DPcount$depth <- c(0:bc1_max_raw)
head(bc1_allGT_GTdiff_DPcount)
tail(bc1_allGT_GTdiff_DPcount)
# for each depth from 0 to max observed in WGS depths (!!! table from step before table building !!!)
for (i in 0:nrow(bc1_allGT_GTdiff_DPcount)-1){
  bc1_allGT_GTdiff_DPcount[which(bc1_allGT_GTdiff_DPcount$depth==i),]['WGS_DP_count'] <- bc1_allGT_GTdiff_WGS_depth[which(bc1_allGT_GTdiff_WGS_depth$WGS_DP==i),]['n']
}

for (i in 0:nrow(bc1_allGT_GTdiff_DPcount)-1){
  bc1_allGT_GTdiff_DPcount[which(bc1_allGT_GTdiff_DPcount$depth==i),]['amp_DP_count'] <- bc1_allGT_GTdiff_amp_depth[which(bc1_allGT_GTdiff_amp_depth$amp_DP==i),]['n']
}
# check the table is correctly filled with values (from start to end)
head(bc1_allGT_GTdiff_DPcount, n=50)
tail(bc1_allGT_GTdiff_DPcount, n=50)

# change NA with 0
bc1_allGT_GTdiff_DPcount[is.na(bc1_allGT_GTdiff_DPcount)] = 0
head(bc1_allGT_GTdiff_DPcount)

# transform table to plot 2 y (counts) on 1 graph
melted_bc1_allGT_GTdiff_DPcount <- melt(bc1_allGT_GTdiff_DPcount, id='depth')
head(melted_bc1_allGT_GTdiff_DPcount)
col_palette <- c(
  'WGS_DP_count' = lightred,
  'amp_DP_count' = lightblue
  )

png(filename="bc1_DPboth.png", width=700, height=500)
ggplot(melted_bc1_allGT_GTdiff_DPcount, aes(x=depth, y=value, fill=variable)) +
  geom_bar(stat = 'summary', position = "identity", alpha=0.7) +
  scale_fill_manual(values=col_palette) +
  geom_text(aes(x=470, y=30, label=paste0('n=', nrow(bc1_allGT_GTdiff)))) +
  labs(title='Counts number by observed depth BC1', subtitle='Genotype different in both WGS and amp', x='Variant depth in WGS and amplicons', y='Number of variants')
dev.off()

# warnings because of droping some data are normal
png(filename="bc1_DPboth_magnified_nolegend_notitle.png", width=700, height=500)
ggplot(melted_bc1_allGT_GTdiff_DPcount, aes(x=depth, y=value, fill=variable)) +
  geom_bar(stat = 'summary', position = "identity", alpha=0.7) +
  geom_vline(xintercept=14.5, linetype="dashed", linewidth=0.5) +
  geom_vline(xintercept=29.5, linetype="dashed", linewidth=0.5) +
  scale_fill_manual(values=col_palette) +
  xlim(0, 100) +
  theme(legend.position="none", axis.title=element_blank(), axis.text=element_text(size=15)) +
  labs(x='Variant depth in WGS and amplicons', y='Number of variants')
dev.off()

### depths according to locus (along 300kb) GTwgs != GTamp
bc1_allGT_GTdiff_DPtechno = data.frame(matrix(0, nrow = length(c(270382778:270675029)), ncol = 3))
colnames(bc1_allGT_GTdiff_DPtechno) <- c("position", "DPWGS", "DPamp")
bc1_allGT_GTdiff_DPtechno$position <- c(270382778:270675029)
for (i in 1:nrow(bc1_allGT_GTdiff)){
  # extract pos of line
  var_pos <- bc1_allGT_GTdiff[i, "POS"]
  if (bc1_allGT_GTdiff[i, "WGS_DP"] == 0 | bc1_allGT_GTdiff[i, "amp_DP"] == 0){next}
  # fill table
  bc1_allGT_GTdiff_DPtechno[which(bc1_allGT_GTdiff_DPtechno$position == var_pos), "DPWGS"] <- bc1_allGT_GTdiff[i, "WGS_DP"]
  bc1_allGT_GTdiff_DPtechno[which(bc1_allGT_GTdiff_DPtechno$position == var_pos), "DPamp"] <- bc1_allGT_GTdiff[i, "amp_DP"]
}
nrow(bc1_allGT_GTdiff_DPtechno[which(bc1_allGT_GTdiff_DPtechno$DPWGS != 0),])
nrow(bc1_allGT_GTdiff_DPtechno[which(bc1_allGT_GTdiff_DPtechno$DPamp != 0),])

# drop pos under threshold
bc1_allGT_GTdiff_DPtechno_thresholdfiltered <- bc1_allGT_GTdiff_DPtechno[which(bc1_allGT_GTdiff_DPtechno$DPWGS>14 & bc1_allGT_GTdiff_DPtechno$DPamp>29),]

# melt dataframe
melt_bc1_allGT_GTdiff_DPtechno_thresholdfiltered <- melt(bc1_allGT_GTdiff_DPtechno_thresholdfiltered, id='position')
head(melt_bc1_allGT_GTdiff_DPtechno_thresholdfiltered)

png(filename="vars_distribution_GTdiffGT_bc1wgs_notitle.png", width=700, height=500)
ggplot(data=na.omit(subset(melt_bc1_allGT_GTdiff_DPtechno_thresholdfiltered[which(melt_bc1_allGT_GTdiff_DPtechno_thresholdfiltered$variable == "DPWGS"),]))) +
  geom_col(aes(x=position, y=value), color='#f54c76') +
  theme(legend.position="none", axis.title=element_blank(), axis.text=element_text(size=20))
dev.off()


#####   Comparison of %hetero along the 300kb of interest BC1 (windows = primers)   #####
### Extract datasets with variants different in WGS and amp and pass threshold (15, 30)
bc1_GT <- bc1_allGT[,c('POS', 'WGS_GT', 'amp_GT')]
head(bc1_GT)

# split GT by "/"
bc1_GT$WGS_GT <- str_split_fixed(bc1_GT$WGS_GT, "/", 2)
bc1_GT$amp_GT <- str_split_fixed(bc1_GT$amp_GT, "/", 2)
head(bc1_GT)
nrow(bc1_GT)  # 6821

# fill homhet column bc1
for ( i in 1:nrow(bc1_GT)){
  if (bc1_GT$WGS_GT[i, 1] == '.' | bc1_GT$WGS_GT[i, 2] == '.'){
    bc1_GT$WGS_homhet[i] <- "."
  }else if (bc1_GT$WGS_GT[i, 1] == bc1_GT$WGS_GT[i, 2]){
    bc1_GT$WGS_homhet[i] <- "hom"
  }else{
    bc1_GT$WGS_homhet[i] <- "het"
  }
  if (bc1_GT$amp_GT[i, 1] == '.' | bc1_GT$amp_GT[i, 2] == '.'){
    bc1_GT$amp_homhet[i] <- "."
  }else if (bc1_GT$amp_GT[i, 1] == bc1_GT$amp_GT[i, 2]){
    bc1_GT$amp_homhet[i] <- "hom"
  }else{
    bc1_GT$amp_homhet[i] <- "het"
  }
}
head(bc1_GT)

# add medium col to name each primers couples by this in plot
primers$med <- primers$start + (primers$stop-primers$start)/2

# bc1 table filling and plotting
compare_bc1_primers <- data.frame(matrix(0, nrow = nrow(primers), ncol = 5))
colnames(compare_bc1_primers) <- c('primer_med', 'WGS_raw_counts', 'WGS_het_perc', 'amp_raw_counts', 'amp_het_perc')
compare_bc1_primers$primers_couple <- NA
for (i in 1:nrow(primers)){
  # fill position column
  compare_bc1_primers$primer_med[i] <- primers$med[i]
  # fill primers_couple column
  compare_bc1_primers$primers_couple[i] <- primers$name[i]
  # create subset according to start/stop positions
  prim_couple_subset_bc1 <- bc1_GT[which(primers$start[i] < bc1_GT$POS & bc1_GT$POS < primers$stop[i]),]
  # fill heterozygous percentage of WGS within the subset
  compare_bc1_primers$WGS_raw_counts[i] <- nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$WGS_homhet != '.'),])
  compare_bc1_primers$WGS_het_perc[i] <- nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$WGS_homhet == 'het'),])*100/nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$WGS_homhet != '.'),])
  # fill heterozygous percentage of amp within the subset
  compare_bc1_primers$amp_raw_counts[i] <- nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$amp_homhet != '.'),])
  compare_bc1_primers$amp_het_perc[i] <- nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$amp_homhet == 'het'),])*100/nrow(prim_couple_subset_bc1[which(prim_couple_subset_bc1$amp_homhet != '.'),])
}
head(compare_bc1_primers)
compare_bc1_primers[which(is.na(compare_bc1_primers$WGS_het_perc)), c('WGS_het_perc', 'amp_het_perc')] = 0
compare_bc1_primers$ratio_perc <- (compare_bc1_primers$WGS_het_perc / compare_bc1_primers$amp_het_perc)/100

# table of percentage
melt_compare_bc1_primers_perc <- melt(compare_bc1_primers[, c(1, 3, 5, 6)], id=c('primer_med', 'primers_couple'))
summary(melt_compare_bc1_primers_perc)

png(filename="bc1_hetperc_primers_cnt_nolegend_notitle.png", width=700, height=500)
ggplot(compare_bc1_primers, aes(x=primer_med)) +
  geom_bar(aes(y=WGS_raw_counts), stat='summary', alpha=0.5) +
  geom_point(aes(y=WGS_het_perc*max(WGS_raw_counts)/100), size=1.5, color='#f54c76') +
  geom_line(aes(y=WGS_het_perc*max(WGS_raw_counts)/100), linewidth=0.75, color='#f54c76') +
  geom_point(aes(y=amp_het_perc*max(WGS_raw_counts)/100), size=1.5, color='#4a44ef') +
  geom_line(aes(y=amp_het_perc*max(WGS_raw_counts)/100), linewidth=0.75, color='#4a44ef') +
  scale_y_continuous(name='%het WGS/amp ratio', sec.axis=(~.*max(c(compare_bc1_primers$WGS_het_perc, compare_bc1_primers$amp_het_perc))/max(compare_bc1_primers$WGS_raw_counts))) +
  theme(legend.position="none", axis.title=element_blank(), axis.text=element_text(size=20))
dev.off()
